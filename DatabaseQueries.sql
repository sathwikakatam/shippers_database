USe sql_store;

select * from customers order by first_name ;
select first_name,last_name,points from customers;
select first_name,last_name,points,points +10 from customers; 
select first_name,last_name,points,(points +10)*100 from customers;
select first_name,
	    last_name,
         points,
         (points +10)*100 as 'discount factor'
         from customers; 
select state from customers;
select distinct state from customers order by state;

 use sql_inventory;
select * from products;
 select name,unit_price ,
         (unit_price *1.1) as 
         'new price' from products;
select * from customers where birth_date >='1990-01-01';
select * from customers where  
            (birth_date>='1990-01-01' 
            or points > 1000) and state = 'VA';

select * from customers where state IN('VA','FL','GA');
select * from customers where points between 1000 and 3000;
select * from customers 
               where address like "trail%" 
               or address like"%avenue%";
select * from customers where phone like "_4%";
select * from customers 
               where address regexp "trail|avenue";
select * from customers where last_name regexp '[gi]e';
select * from customers where last_name regexp '[a-h]e';
select * from customers where phone is  null;
select * from customers where phone is not null;
-- sorting data
select * from customers  order by state desc,first_name;

select * ,(quantity *unit_price) as total_price
	from order_items where order_id=2 order by total_price desc;
-- limiting the queries
select * from customers limit 3;
select * from customers limit 4,3; -- 4 is offset i.e skips first 4
select * from customers order by points desc limit 3;
-- limit always comes at the end
-- INNER JOINS
-- MULTIPLES TABLES;
SELECT * FROM ORDERS JOIN customers ON 
 orders.customer_id = customers.customer_id;
 -- if the more than two tables have same column 
 -- then we have to prefix it with table name
SELECT first_name,last_name,order_id FROM 
  ORDERS o JOIN customers c ON 
  o.customer_id = c.customer_id;
select o.product_id,order_id,name,o.unit_price,quantity_in_stock
     from order_items o join products p 
     on o.product_id = p.product_id;
-- if there is alias we shoukd always use that 
-- alias name instead of table name
-- joing accross databases
select *  from order_items oi join 
      sql_inventory.products p on p.product_id =oi.product_id;
-- self Joinss
use sql_hr;
select e.employee_id,e.first_name,m.first_name as manager
  from employees e 
   join employees m 
   on e.reports_to =m.employee_id;
   
--  joining multiple tables
use invoicing;
select p.payment_id,
      c.client_id,
      i.invoice_id,
      invoice_date as date,
      payment_total as amount,
      payment_method
 from clients c 
     join invoices i on 
        c.client_id= i.invoice_id
	 join payments p 
     on i.client_id =p.payment_id;
     
	-- compound join conditions
    -- composite primary key has more than one column
    use sql_store;
select * from order_items oi
  join order_item_notes oin
  on oi.order_id = oin.order_id
  and oi.product_id = oin.product_id;
-- implicit joins using where
-- some times these cause cross joins ie one column in one table matches which each other columns

-- OUTER JOINS
   -- left joins
   -- right joins
   use sql_store;
   
   select p.product_id,name,quantity from products p
    left join order_items oi on 
    p.product_id =oi.product_id;
-- outer joins between multiple tables

select o.order_id,o.order_date,c.first_name as customer,
      sh.name as shipper, os.name as status 
      from orders o join customers c 
      on o.customer_id = c.customer_id
      left join shippers sh
      on o.shipper_id = sh.shipper_id
      join order_statuses os
       on o.status = os.order_status_id;
       
-- using 
   -- instead of on condition when can specify 'using'
   --  this is used only the two column names are same and we can also pass compisite keys i
   
   use invoicing;
   select c.name,date,amount,pm.name as 'payment mode' from payments p join clients c
       using(client_id) join invoices i
       using (invoice_id) 
       join payment_methods pm on 
       pm.payment_method_id=p.payment_method ;

-- natural join
   -- database naturally looks for common columns  and joins the tables
    use  sql_store;
   select * from orders o natural join customers c;
   
   -- union 
     -- combine results from multiple  query
     -- column name id given based on first query
select first_name,points ,'Bronze' as type from customers 
       where points <2000
       union 
select first_name, points ,'slver' as type from customers 
   where points between 2000 and 3000
   union
select first_name,points,'Gold' as type from customers
  where points between 2000 and 3000 order by first_name;
  
  -- copying the data from one table to another new table
  create table order_copy AS select * from orders;
  use invoicing;
  create table invoice_copy as
  select i.invoice_total,
         i.payment_total,
         c.name,
         i.payment_date,
         i.due_date
      from  
        invoices i 
         join clients c using (client_id)
         where payment_date is not null; 
         drop table invoice_copy;
drop table order_copy;
  -- dropinvoices table remove table defination .Trucate table will just clear data from table but will keep data
  -- this will createe the another table with data in order but don't mark primary key as primary and also ignore auto increment feature


-- Updating rows
update invoices
set payment_total =10,payment_date = '2019-01-09' where invoice_id =3;

USE sql_store;
select * from customers;
UPDATE customers
set points =points+50 where  birth_date < '1990-01-01';

-- subqueries with UPDATE
-- subquery is a select statement inside another sql query
use invoicing;
update invoices
set 
  payment_total = invoice_total * 0.5,
    payment_date = due_date
where client_id IN (select client_id from clients
                    where state IN ('ca','ny'));
use sql_store;
UPDATE orders 
set comments = 'gold customers' 
where customer_id IN (
select customer_id from 
       customers c 
       where c.points>3000);
-- DELETE ROWS
-- delete is used to delete all the rows from table
-- difference between delete and truncate is truncate always delete all the rows 
-- where as delete removes the rows conditionally

-- AGGREGATE FUNCTIONS
-- max() min() avg() sum() count()
-- It works on dates and strings too on date it give latest date
-- It doesn't work on null values to count null values use count(*)
use invoicing;
select 'first half of 2019' as date_range,
       sum(invoice_total) as total_sales,
       sum(payment_total) as total_payments,
       sum(invoice_total- payment_total) as 'what we expect'
       from invoices where invoice_date 
        between '2019-01-01' and '2019-06-30'
UNION
select 'second half of 2019' as date_range,
       sum(invoice_total) as total_sales,
       sum(payment_total) as total_payments,
       sum(invoice_total- payment_total) as 'what we expect'
       from invoices where invoice_date 
        between '2019-06-30' and '2019-12-31'
UNION
select 'total' as date_range,
       sum(invoice_total) as total_sales,
       sum(payment_total) as total_payments,
       sum(invoice_total- payment_total) as 'what we expect'
       from invoices where invoice_date 
        between '2019-01-01' and '2019-12-31';
select 
  max(invoice_total) as highest,
  min(invoice_total) as lowest,
  avg(invoice_total) as average,
  sum(invoice_total) as total,
  count(DISTINCT client_id) as total_records
  from invoices;
  
-- GROUP BY : used to group data based of two of more coulms
--  group by is always after from or where and order by is always after group by if we use

select client_id,
      sum(invoice_total) as total from invoices
      where invoice_date >= "2019-07-01"
      group by client_id order by total desc;
select date,name,sum(amount) as total,
     count(*) as number_of_invoices from 
         payments p join payment_methods pm
          on p.payment_method = pm.payment_method_id
group by date,name  having total > 50  and number_of_invoices >1
order by date ;
-- Having : to filter data after grouping and always follows group by
-- using 'where' we can write condition irrespective of we select them or not
-- but using having we can write only if we select them
use sql_store; 
select c.customer_id,first_name,last_name,
        sum(oi.quantity*oi.unit_price) as total  from customers c join 
            orders o using (customer_id)
            join
            order_items oi using (order_id)
 where state ='VA'
 group by c.customer_id,first_name,last_name having total >100;
 
 -- ROLL Up  : summarizes all the data and it applies only to aggreagate functions 
 -- if we have multiple coulmns we see roll up for each group
 
 
 
 


  











  
  
  
     
     
   

       

   


      
   


  
    



   






 



 






 

